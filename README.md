# mchart

![](https://img.shields.io/badge/written%20in-PHP-blue)

A minimal charting library.

- Supports line (connected scatter), staggered line, and bar charts with PNG output.
- Depends on php5-gd only (yes that means those terrible builtin fonts).
- Single-file and easily extensible, intended for bundling in throwaway scripts.

Tags: graphics


## Download

- [⬇️ MChart-r10.tar.gz](dist-archive/MChart-r10.tar.gz) *(2.18 KiB)*
